/* 
 This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *  \brief     This is a OpenSCAD model of a Ugrinsky wind turbine.
 *  \details   It is highly inspired by this thing on Thingiverse (https://www.thingiverse.com/thing:996946). Especially the engineering drawing there was helpful. Beside this link I could unfortunately not find many background information.
 *  \author    Kai Kientopf
 *  \version   1.0
 *  \date      2020
 *  \copyright GNU Public License.
 */


R = 100;
height = 100;
thickness = 2;
thickness_minimum = 1;

screw_diameter = 2.7;
screw_hole_diameter = 2.2;
screw_head_diameter = 5;
screw_head_height = 2;
screw_length = 13;

bearing_outer_diameter = 22;
bearing_inner_diameter = 8;
bearing_height = 7;
bearing_nut_diameter = 18.4;
bearing_contact_diameter = bearing_nut_diameter;

tolerance = 0.25;

$fa = 6;

//top();
//base();
ugrinsky();
//bearing_spacer();
//bearing_holder();

/**
 * basic geometry out of multiple circles
 * @param R the main radius of the wind turbine
 * @param h the height of the wind turbine
 * @param cor a correction value to make the geometry smaller or thicker
 */
module ugrinsky_geometry(R=R, h=height, cor=0)
{
    // symmetric
    for(i = [-1, 1])
    {
        difference()
        {
            // the main circle
            cylinder(r=R+cor, h=h);
            // reduce it to half circle
            translate([0, i*R/2, h/2])
            {
                cube([2*R, R, h], center=true);
            }
            // cut out big circle
            translate([i*(-1)*0.8*R, 0, 0])
            {
                cylinder(r=R-cor, h=h);
            } 
        }
        // add small circle
        translate([i*0.6*R, 0, 0])
        {
            cylinder(r=0.4*R+cor, h=h);
        }
    }
}

/**
 * the vertical walls of the wind turbine
 * @param R the main radius of the wind turbine
 * @param h the height of the wind turbine
 * @param t the thickness of the walls
 */ 
module ugrinsky_walls(R=R, h=height, t=thickness)
{
    intersection()
    {
        // get the walls by cut out a smaller geometry out of a bigger geometry
        difference()
        {   
            ugrinsky_geometry(R=R, h=h, cor=t/2);
            ugrinsky_geometry(R=R, h=h, cor=-t/2);
        }
        // cut out the important part
        union()
        {
            // all the outer part is not important
            cylinder(r=R-t/2, h=h);
            // ... except ...
            for(i = [-1, 1])
            {
                // the small circle
                intersection()
                {
                    translate([i*0.6*R, 0, 0])
                    {
                        cylinder(r=0.4*R+t/2, h=h);
                    }
                    translate([i*(R+t/2)/2, i*0.4*R/2, h/2])
                    {
                        cube([R+t/2, 0.4*R, h], center=true);
                    }   
                }
                // the gib circle
                intersection()
                {
                    translate([i*(-1)*0.8*R, 0, 0])
                    {
                        cylinder(r=R+t/2, h=h);
                    } 
                    translate([i*(-1)*(R+t/2)/2, i*(-1)*((R+t/2)/2+R/4), h/2])
                    {
                        cube([R+t/2, R/2+t/2, h], center=true);
                    }
                }
            }
        }
    }
}

/**
 * the horizontal walls of the wind turbine
 * @param R the main radius of the wind turbine
 * @param t the thickness of the walls
 */
module ugrinsky_base(R=R, t=thickness)
{
    difference()
    {
        // main circle
        cylinder(r=R+t/2, h=t);
        // without the geometry
        ugrinsky_geometry(R=R+t/2, h=t, cor=0);
    }
    // but with the part of the walls
    ugrinsky_walls(R=R, h=t, t=t);
}

/**
 * connect all parts together
 * @param R the main radius of the wind turbine
 * @param h the height of the wind turbine
 * @param t the thickness of the walls
 * @param top show the top wall or not
 */
module ugrinsky(R=R, h=height, t=thickness, top=true)
{
    difference()
    {
        union()
        {
            ugrinsky_walls(R=R, h=h-t, t=t);
            ugrinsky_base(R=R, t=t);
            translate([0, 0, h-t])
            {
                ugrinsky_base(R=R, t=t);
            }
        }
        if(!top)
        {
            translate([0, 0, h-t])
            {
                ugrinsky_base(R=R, t=t);
            }
        }
    }
}

/**
 * calculate the position for screws along the vertical walls
 * @param R the main radius of the wind turbine
 * @param t the thickness of the walls
 * @param num defines the numbers on one circle part - in total 2*(2*num-1)
 */
module ugrinsky_holes(R=R, t=thickness, num=3)
{
    // symmetric
    for(i = [-1, 1])
    {
        // small circle
        translate([i*(0.6*R), 0, 0])
        {
            // calculates the start
            x = (180/(PI*0.4*R))*2*t/3;
            // angle along the circle
            for(a = [x:(180-x)/num:180])
            {
                rotate([0, 0, a])
                {
                    translate([i*(0.4*R), 0, 0])
                    {
                        children();
                    }
                }
            }
        }
        // big circle
        translate([i*(0.8*R), 0, 0])
        {
            // calculates the start
            x = (180/(PI*R))*t/2;
            // angle along the circle
            for(a = [112+x:(68-x)/num:180])
            {
                rotate([0, 0, a])
                {
                    translate([i*(R), 0, 0])
                    {
                        children();
                    }
                }
            }
        }
    }
}

/**
 * a screwable top part with vertival walls
 */
module top()
{
    difference()
    {
        union()
        {
            ugrinsky(top=false);
            if(thickness<screw_head_diameter+2*thickness_minimum)
            {
                translate([0, 0, height])
                {
                    ugrinsky_holes(num=2, t=screw_diameter+2*thickness)
                    {
                        difference()
                        {
                            screw(housing=true);
                            translate([0, 0, -thickness])
                            {
                                cylinder(d=screw_head_diameter+2*thickness, h=thickness, $fn=20);
                            }
                        }
                    }
                }
            }
        }
        translate([0, 0, height])
        {
            ugrinsky_holes(num=2, t=thickness<screw_head_diameter+2*thickness_minimum?screw_diameter+2*thickness:thickness)
            {
                screw(self_cut=true);
            }
        }
    }
}

/**
 * a screwable base part
 */
module base()
{
    difference()
    {
        union()
        {
            ugrinsky_base();
            if(thickness<screw_head_diameter+2*thickness_minimum)
            {
                ugrinsky_holes(num=2, t=screw_diameter+2*thickness)
                {
                    difference()
                    {
                        translate([0, 0, thickness])
                        {
                            screw(housing=true);
                        }
                        screw(housing=true);
                    }
                }
                bearing_holder_holes(start=45)
                {
                    translate([0, 0, 2*thickness])
                    {
                        difference()
                        {
                            screw(housing=true, housing_tip=false);
                            translate([0, 0, -thickness])
                            {
                                cylinder(d=screw_head_diameter+2*thickness, h=thickness, $fn=20);
                            }
                        }
                    }
                }
            }
        }
        ugrinsky_holes(num=2, t=thickness<screw_head_diameter+2*thickness_minimum?screw_diameter+2*thickness:thickness)
        {
            translate([0, 0, thickness])
            {
                screw();
            }
        }
        bearing_holder_holes(start=45)
        {
            translate([0, 0, 2*thickness])
            {
                screw();
            }
        }
    }
}

module screw(self_cut=false, flip=false, housing=false, housing_tip=true)
{
    diameter = (self_cut && !housing)?screw_hole_diameter:screw_diameter;
    cor = housing?2*thickness:0;
    r = flip?[180, 0, 0]:[0, 0, 0];
    $fn=20;
    
    rotate(r)
    {
        translate([0, 0, -screw_head_height])
        {
            cylinder(d1=screw_diameter+cor, d2=screw_head_diameter+cor, h=screw_head_height);
        }
        translate([0, 0, -screw_length])
        {
            cylinder(d=diameter+cor, h=screw_length-screw_head_height);
        }
        if(housing)
        {
            if(housing_tip)
            {
                translate([0, 0, -screw_length-2*(diameter+cor)])
                {
                    cylinder(d1=0, d2=diameter+cor, h=2*(diameter+cor));
                }
            }
            else
            {
                translate([0, 0, -screw_length-thickness])
                {
                    cylinder(d1=0, d2=diameter+cor, h=thickness);
                }
            }
        }
    }
}

//!screw(self_cut=false, flip=true, housing=false);

module bearing_holder_main()
{
    difference()
    {
        union()
        {
            cylinder(d=bearing_outer_diameter+2*thickness, h=bearing_height+2*thickness);
            cylinder(d=bearing_outer_diameter+2*thickness+2*(screw_head_diameter+thickness_minimum), h=thickness);
        }
        cylinder(d=bearing_outer_diameter+2*tolerance, h=bearing_height+thickness);
        cylinder(d=bearing_nut_diameter, h=bearing_height+2*thickness);
    }
}

module bearing_holder_holes(num=4, start=0)
{
    for(a = [start:360/num:start+360])
    {
        rotate([0, 0, a])
        {
            translate([bearing_outer_diameter/2+thickness+screw_head_diameter/2, 0, 0])
            {
                children();
            }
        }
    }
}

module bearing_holder()
{
    difference()
    {
        bearing_holder_main();
        bearing_holder_holes()
        {
            translate([0, 0, thickness])
            {
                screw();
            }
        }
    }
}

module bearing_spacer()
{
    difference()
    {
        cylinder(d=bearing_outer_diameter, h=thickness-2*tolerance);
        cylinder(d=bearing_contact_diameter, h=thickness-2*tolerance);
    }
}